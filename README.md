# ircdeez
ircdeeznuts, find yourself some ircd's

# installation
```
git clone https://git.supernets.org/delorean/ircdeez.git
go mod tidy
go build .
./ircdeez -h
```

# usage
```
-r  |  CIDR range to scan
-l  |  list of addresses to scan
-p  |  ip:port socks5 proxy list
-i  |  ircd port [6667]
-n  |  number of proxies to cycle per ip [1]
-t  |  threads [100]
```

```
ircdeez -r 0.0.0.0/0 -p socks.txt -n 3 -t 1000
```