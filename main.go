// ircdeeznuts
//
// go build . && ./ircdeez -h
//
// sincerely,
//      delorean

package main

import (
	"bufio"
	"flag"
	"fmt"
	"math/rand"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	"h12.io/socks"
)

type Proxy struct {
	Addr string
	Port int
}

var (
	proxylist = flag.String("p", "", "socks5 proxy list")
	rangearg  = flag.String("r", "", "ip range cidr")
	list      = flag.String("l", "", "target list")
	ircdport  = flag.Int("i", 6667, "ircd port")
	retries   = flag.Int("n", 1, "number of proxy retries per target address")
	threads   = flag.Int("t", 100, "threads")
)

var proxies []Proxy
var proxying = false

func pullproxies(path string) ([]Proxy, error) {
	var arr []Proxy

	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	s := bufio.NewScanner(f)
	for s.Scan() {
		p := strings.Split(s.Text(), ":")
		if len(p) != 2 {
			continue
		}

		port, e := strconv.Atoi(p[1])
		if e != nil {
			continue
		}
		arr = append(arr, Proxy{Addr: p[0], Port: port})
	}

	return arr, nil
}

func readbanner(c net.Conn) (string, error) {
	buf := make([]byte, 2048)
	c.SetReadDeadline(time.Now().Add(time.Second * 10))
	numread, err := c.Read(buf)
	if err != nil {
		return "", err
	}
	banner := string(buf[0:numread])
	return banner, nil
}

func proxydial(addr string, port int, rproxy Proxy) error {
	proxydest := fmt.Sprintf("socks5://%s:%d?timeout=5s", rproxy.Addr, rproxy.Port)
	dialSocksProxy := socks.Dial(proxydest)

	dest := fmt.Sprintf("%s:%d", addr, port)
	conn, e := dialSocksProxy("tcp", dest)
	if e != nil {
		return e
	}

	banner, e := readbanner(conn)
	if e != nil {
		return e
	}

	if strings.Contains(banner, "NOTICE") && !strings.Contains(strings.ToLower(banner), "bitlbee") {
		fmt.Println(addr)
	}

	conn.Close()
	return nil

}

func rawdog(addr string, port int) error {
	dest := fmt.Sprintf("%s:%d", addr, port)
	conn, e := net.DialTimeout("tcp", dest, time.Second*5)
	if e != nil {
		return e
	}

	banner, e := readbanner(conn)
	if e != nil {
		return e
	}

	if strings.Contains(banner, "NOTICE") {
		fmt.Println(addr)
	}

	conn.Close()
	return nil
}

func randproxy() Proxy {
	return proxies[rand.Intn(len(proxies))]
}

func runCIDR(cidr string, out chan string) error {
	ip, ipnet, err := net.ParseCIDR(cidr)
	if err != nil {
		return err
	}

	for target := ip.Mask(ipnet.Mask); ipnet.Contains(target); inc(target) {
		addr, _ := net.ResolveIPAddr("ip", target.String())
		out <- addr.String()
	}
	return nil
}

func inc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func thread(in <-chan string, tracker chan<- interface{}) {
	for ip := range in {
		c := 0
		if proxying {
			for {
				if c == *retries {
					break
				}
				p := randproxy()
				if e := proxydial(ip, *ircdport, p); e == nil {
					break
				}
				c++
			}
		} else {
			_ = rawdog(ip, *ircdport)
		}
	}

	tracker <- 1337
}

func usage() {
	fmt.Printf(`ircdeeznuts
  -r <cidr>		ipv4 cidr range to scan
  -l <file>		target ip's
  -p <file>		proxy list
  -i <port>		port for ircd's [6667]
  -n <num>		# of proxies to cycle per failure [3]
  -t <num>		threads [100]
`)
}

func fatal(e string) {
	fmt.Printf("fatal: %s\n", e)
	os.Exit(-1)
}

func main() {
	flag.Usage = usage
	flag.Parse()

	if *list == "" && *rangearg == "" {
		fatal("provide either a list of ip's (-l) or a cidr range (-r)")
	}

	if *list != "" && *rangearg != "" {
		fatal("received both a range and a list, one at a time nigga")
	}

	if *proxylist != "" {
		temp, e := pullproxies(*proxylist)
		if e != nil {
			fatal(e.Error())
		}

		if len(temp) < 1 {
			fatal("no valid proxies retrieved")
		}

		proxies = temp
		proxying = true
	}

	ips := make(chan string)
	tracker := make(chan interface{})
	rand.Seed(time.Now().Unix())

	for x := 0; x < *threads; x++ {
		go thread(ips, tracker)
	}

	if *rangearg != "" {
		e := runCIDR(*rangearg, ips)
		if e != nil {
			fatal("error parsing cidr range")
		}
	} else {
		fd, e := os.Open(*list)
		if e != nil {
			fatal(e.Error())
		}
		defer fd.Close()
		fs := bufio.NewScanner(fd)

		for fs.Scan() {
			line := strings.ReplaceAll(fs.Text(), " ", "")
			if len(line) > 0 {
				ips <- line
			}
		}
	}

	close(ips)

	for x := 0; x < *threads; x++ {
		<-tracker
	}

}
